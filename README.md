# Build TensorFlow, Jupiter and Nvidia Toolkit with Docker Compose

## Slide Link:

> https://docs.google.com/presentation/d/1NW4dleoXyYt4cnuNxbfnwbQ4xn6lzTf3iKTgrT9BX8s/edit?usp=sharing


## Build with Docker 


*  **List of TF docker image version**

> https://hub.docker.com/r/tensorflow/tensorflow/tags



*  **Stable latest version of TF**
```
1.13.2-gpu-py3-jupyter 2 GB
Last update: 17 days ago
```


*  **Beta latest version of TF**
```
2.0.0b1-gpu-py3-jupyter 2 GB
Last update: 2 months ago
```

*  **Pull docker image of stable & beta latest version of TF**

> docker pull tensorflow/tensorflow:1.13.2-gpu-py3-jupyter

> docker pull tensorflow/tensorflow:2.0.0b1-gpu-py3-jupyter


* **Run Jupiter service that includes TF and Nvidia ToolKit**

Linux like OS:

> docker run -it --rm -v /tmp/tf2:/tf/notebooks -p 8888:8888 tensorflow/tensorflow:2.0.0b1-gpu-py3-jupyter

> docker run -it --rm -v /tmp/tf2:/tf/notebooks -p 8888:8888 tensorflow/tensorflow:1.13.2-gpu-py3-jupyter


Windows OS:

> docker run -it --rm -v C:\tf2:/tf/notebooks -p 8888:8888 tensorflow/tensorflow:2.0.0b1-gpu-py3-jupyter


> docker run -it --rm -v C:\tf2:/tf/notebooks -p 8888:8888 tensorflow/tensorflow:1.13.2-gpu-py3-jupyter

```
________                               _______________                
___  __/__________________________________  ____/__  /________      __
__  /  _  _ \_  __ \_  ___/  __ \_  ___/_  /_   __  /_  __ \_ | /| / /
_  /   /  __/  / / /(__  )/ /_/ /  /   _  __/   _  / / /_/ /_ |/ |/ / 
/_/    \___//_/ /_//____/ \____//_/    /_/      /_/  \____/____/|__/


WARNING: You are running this container as root, which can cause new files in
mounted volumes to be created as the root user on your host machine.

To avoid this, run the container by specifying your user's userid:

$ docker run -u $(id -u):$(id -g) args...

[I 10:21:38.982 NotebookApp] Writing notebook server cookie secret to /root/.local/share/jupyter/runtime/notebook_cookie_secret
jupyter_http_over_ws extension initialized. Listening on /http_over_websocket
[I 10:21:40.116 NotebookApp] Serving notebooks from local directory: /tf
[I 10:21:40.116 NotebookApp] The Jupyter Notebook is running at:
[I 10:21:40.116 NotebookApp] http://(e8f36bca4c61 or 127.0.0.1):8888/?token=94998c70c5f27aef80015038798cfe5ba7b0c65a3a81d828
[I 10:21:40.116 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 10:21:40.121 NotebookApp] 
    
    To access the notebook, open this file in a browser:
        file:///root/.local/share/jupyter/runtime/nbserver-1-open.html
    Or copy and paste one of these URLs:
        http://(e8f36bca4c61 or 127.0.0.1):8888/?token=94998c70c5f27aef80015038798cfe5ba7b0c65a3a81d828
```

* **Jupiter Port**
> 8888

* **Port conflict**
> docker run -it --rm -v /tmp/tf2:/tf/notebooks -tensorflow:2.0.0b1-gpu-py3-jupyterp 1919:8888 tensorflow/

* **Surf your Jupiter service**
> http://localhost:8888

* **Fill your token to "Password or toke" field**

e.g. Willis' Jupiter token as above

> 94998c70c5f27aef80015038798cfe5ba7b0c65a3a81d828

* **Click "Log in" button and then it will redirect to below link**

> http://localhost:8888/tree/notebooks#notebooks

## Build with Docker Compose

* **Download code**

> https://gitlab.com/ioinfinity-play/docker-jupiter-tf-gpu/-/archive/master/docker-jupiter-tf-gpu-master.zip

or using git command to download

> git clone https://gitlab.com/ioinfinity-play/docker-jupiter-tf-gpu.git

* **Change directory into docker-jupiter-tf-gpu/docker-compose**

> cd docker-jupiter-tf-gpu/docker-compose

* **Run Jupiter with Docker Compose according to TF version** 

> sudo docker-compose -f docker-compose.tf2.beta.yaml up 

or 

> sudo docker-compose -f docker-compose.tf1.latest.yaml up 



